// Conway's Game of Life in Go.
//
// This implementation uses a go routine for each cell.  The cells
// inform their neighbors about their state via a channel; we have 8
// channels per cell (one for each neighbor).  We assume a
// vt100-compatible output device, and print "*" for a live cell and "
// " for a dead cell.  The InitialGrid constant contains the starting
// position.  The grid wraps at the edges.
//
// This is not the most efficient way to implement Conways's Game of
// Life.  This program was used as an excercise to learn Go.  It is
// likely not a good Go program; one week ago I did not know anything
// about the syntax of Go, so there are likely to be a lot of
// beginners mistakes.

package main

import (
    "fmt"
    "os"
    "regexp"
    "strconv"
    "golang.org/x/term"
)

var (
    // We start with one glider, one static block, and two oscillators
    // in the top left corner.
    InitialGrid []string = []string{
	" *                         ** ",
	"  *       **    ***        **  ",
	"***       **                 **",
        "                             **"}
    GridRows, GridCols int
)

type Message struct {
    origin_x, origin_y int
    index int
    alive bool
    gen int
}

type Cell struct {
    x, y int
    alive bool
    neighbor_alive *[8]<-chan Message
    output *[8]chan<- Message
}

type StatusUpdate struct {
    x, y int
    alive bool
}

var grapher chan StatusUpdate

func run(cell *Cell) {
    // Display the initial state.
    grapher <- StatusUpdate{cell.x, cell.y, cell.alive}

    generation := 0

    for {
	generation++
	// Broadcast our state.
	for i := 0; i < 8; i++ {
	    cell.output[i] <- Message{cell.x, cell.y, i, cell.alive, generation}
	}

	// Count living neighbors.
	living := 0
	for i := 0; i < 8; i++ {
	    msg := <-cell.neighbor_alive[i]
	    if msg.alive {
		living++
	    }
	}

	// Update our status.
	old_alive := cell.alive
	if cell.alive {
	    if living < 2 || living > 3 {
		cell.alive = false
	    }
	} else {
	    if living == 3 {
		cell.alive = true
	    }
	}

	// Inform the grapher
	if cell.alive != old_alive {
	    grapher <- StatusUpdate{cell.x, cell.y, cell.alive}
	}
    }
}

func gen_0_alive(x, y int) bool {
    if x >= len(InitialGrid) {
	return false
    }

    if y >= len(InitialGrid[x]) {
	return false
    }

    if InitialGrid[x][y:y+1] == " " {
	return false
    }

    return true
}

func Start() {
    cells := GridRows * GridCols
    the_cells := make([]*Cell, cells)

    // Allocate the cells.  For now, all channels are nil.
    for ix := 0; ix < GridRows; ix++ {
	for iy := 0; iy < GridCols; iy++ {
	    pos := ix + GridRows * iy

	    the_cells[pos] = &Cell{ix, iy, gen_0_alive(ix, iy),
		new([8]<-chan Message),
		new([8]chan <-Message)}
	}
    }

    // Connect each cell to its neighbors.
    for ix := 0; ix < GridRows; ix++ {
	for iy := 0; iy < GridCols; iy++ {
	    pos := ix + GridRows * iy

	    // The connect function injects one channel.
    	    connect := func(dx, dy, n int) {
		x := (ix + dx) % GridRows
		y := (iy + dy) % GridCols
		if x < 0 {
		    x += GridRows
		}
		if y < 0 {
		    y += GridCols
		}
		pos2 := x + GridRows * y

		a := make(chan Message, 1)

		the_cells[pos].neighbor_alive[n] = a
		if the_cells[pos2].output[n] != nil {
		    panic("internal output error n\n")
		}
		the_cells[pos2].output[n] = a
	    }

	    connect(-1, -1, 0)
	    connect(-1, 0, 1)
	    connect(-1, 1, 2)
	    connect(0, -1, 3)
	    connect(0, 1, 4)
	    connect(1, -1, 5)
	    connect(1, 0, 6)
	    connect(1, 1, 7)
	}
    }

    // Start all the goroutines, one per cell.
    for _, c := range(the_cells) {
	go run(c)
    }
}

// Read a string from stdin in raw mode.  Restore the mode.  We only
// use this once, to read the screen size report.  If we used this
// multiple times, it would be inefficent to switch the mode every
// time we use this function.
func read_stdin() ([]byte, error) {
    oldState, err := term.MakeRaw(int(os.Stdin.Fd()))
    if err != nil {
        panic(err)
    }
    defer term.Restore(int(os.Stdin.Fd()), oldState)

    buf := make([]byte, 100)

    n, err := os.Stdin.Read(buf)
    return buf[:n], err
}

// Probe the screen size.
func find_screen_size() (rows, cols int) {
    print("\033[18;t")	    // Send "Report window test size" request.

    // Read the response, which should be on the form "ESC [ 7 ; R ; C t".
    // But urxvt-unicode responds with 8, not 7; handle both.
    buf, err := read_stdin()
    if err != nil {
	panic("cannot handle errors reading stdin")
    }

    report_re := regexp.MustCompile("\033\\[[78];(?P<row>\\d+);(?P<col>\\d+)t")
    matches := report_re.FindSubmatch(buf)
    rows, rerr := strconv.Atoi(string(matches[1]))
    cols, cerr := strconv.Atoi(string(matches[2]))
    if rerr != nil || cerr != nil {
	panic("number conversion error")
    }
    return
}
func main() {
    GridRows, GridCols = find_screen_size()

    grapher = make(chan StatusUpdate)
    Start()

    // For each report from a cell, update the corresponding position
    // on the terminal.
    for s := range grapher {

	// Move to the correct position.  (Position 1, 1 is the top
	// left corner, but our program uses base 0, so we have to add
	// 1 to get rows and columns.)
	fmt.Printf("\033[%d;%dH", s.x + 1, s.y + 1)
	if s.alive {
	    fmt.Printf("*")
	} else {
	    fmt.Printf(" ")
	}
    }
}
